# Maintainer ~ kyle[dot]devir[at]mykolab[dot]com
# Contributor: Luca Weiss <luca (at) z3ntu (dot) xyz>

pkgname=maya
pkgver=2018_6_UPDATE
_pkgver=2018_6_Update
pkgrel=1
pkgdesc="Autodesk Maya 3D modelling software suite"
arch=('x86_64')
url="http://www.autodesk.com/products/maya/overview"
license=('custom: unlimited')
depends=('libpng12' 'tcsh' 'libxp' 'openssl' 'libjpeg6-turbo' 'libtiff' 'gamin'
         'audiofile' 'e2fsprogs' 'xorg-fonts-75dpi' 'xorg-fonts-100dpi'
         'xorg-fonts-misc' 'openssl-1.0' 'qt4')
install="maya.install"
options=(!strip)
source=("local://Autodesk_Maya_${_pkgver}_Linux_64bit.tgz"
        'maya.desktop')
sha512sums=('093d247cdf07abfc6858548d5402a61c059f903e8c7bff01f31f48882c933b4b5b9f126a5ced85b888034c3dc5b5eb88a60045a4925d3f9b546aef94cb274f20'
            'baf4d83dc8add3866fb02ae83f71429eb9add5c6ca3c873e30520770069a714ec5e3e438b877c0b5657a928f868100bf6a526fcc710592b25ade7f7d98c4913e')

prepare() {
    cd "$srcdir"
    
    rm -Rf ../maya-setup
    mkdir -p ../maya-setup
    mv * ../maya-setup
}

package() {
    cd "$pkgdir"
    
    # Extract RPMs
    for i in ../../maya-setup/*.rpm; do
        msg2 "Extracting ${i}"
        bsdtar -xf $i
    done
    
    mkdir "$pkgdir"/usr/tmp/
    chmod 777 "$pkgdir"/usr/tmp/
    
#    mkdir -p "$pkgdir"/usr/lib/
#    chmod 755 "$pkgdir"/usr/lib/
#    cp "$pkgdir"/opt/Autodesk/Adlm/R12/lib64/libadlmPIT.so.12  "$pkgdir"/usr/lib/libadlmPIT.so.12
#    cp "$pkgdir"/opt/Autodesk/Adlm/R12/lib64/libadlmutil.so.12 "$pkgdir"/usr/lib/libadlmutil.so.12
    
    ln -s /usr/lib/libssl.so.1.0.0 "$pkgdir"/usr/autodesk/maya2018/lib/libssl.so.10
    ln -s /usr/lib/libcrypto.so.1.0.0 "$pkgdir"/usr/autodesk/maya2018/lib/libcrypto.so.10
    ln -s /usr/lib/libjpeg.so.62 "$pkgdir"/usr/autodesk/maya2018/lib/libjpeg.so.62
    ln -s /usr/lib/libtiff.so "$pkgdir"/usr/autodesk/maya2018/lib/libtiff.so.3
    
    mkdir -p "$pkgdir"/usr/bin/
    chmod 755 "$pkgdir"/usr/bin/
    ln -s /usr/autodesk/maya2018/bin/maya2018 "$pkgdir"/usr/bin/maya2018
    
    mkdir -p "$pkgdir"/usr/share/applications/
    chmod 755 "$pkgdir"/usr/share/applications/
    install -Dm644 ../../maya-setup/maya.desktop "$pkgdir"/usr/share/applications/maya.desktop

    mkdir -p "$pkgdir"/opt/maya-setup/
    chmod 755 "$pkgdir"/opt/maya-setup/
    cp ../../maya-setup/setup{,.xml} "$pkgdir"/opt/maya-setup/
}
